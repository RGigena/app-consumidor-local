﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class TiendaModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sector { get; set; }
        public string Tipo { get; set; }
    }
}
