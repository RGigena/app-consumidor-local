﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class ItemModel
    {
        public string idVendingDetalleCarrito { get; set; }
        public string idExpSel { get; set; }
        public int amount { get; set; }
        public decimal price { get; set; }
        public string description { get; set; }
        public string brand { get; set; }
        public string barcode { get; set; }
        public string store { get; set; }
        public string size { get; set; }
        public string unit { get; set; }
        public int stock { get; set; }
        public bool consumido { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public int id { get; set; }
        public DateTime dateTime { get; set; }
        public List<TiendaModel> tienda { get; set; }
        public List<MovimientosModel> movimientos { get; set;}

    }
}
