﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class CompraModel
    {
        public string qrUrl {  get; set; }
        public CartModel Cart { get; set; }
    }
}
