﻿using Gherkin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class BodyModel
    {
        public int Status { get; set; }
        public string? id { get; set; }
        public string? store { get; set; }


        [JsonProperty(PropertyName = "balance")]
        public float Balance { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public string Amount { get; set; }

        [JsonProperty(PropertyName = "idVendingConsumidorEstado")]
        public string IdVendingConsumidor { get; set; }



        public List<JObject> Body { get; set; }

        public TokenModel Token { get; set; }
        public UserModel User { get; set; }
        public CartModel Cart { get; set; }
        public List<TiendaModel> tienda { get; set; }
        public List<ItemModel> items { get; set; }
        
    }
}
