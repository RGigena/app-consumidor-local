﻿using BaseFramework.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api
{
    public class TransactionsClient : BaseClient
    {
        public TransactionsClient(string baseUrl, string token = "") : base(baseUrl, token)
        {
        }

        //Obtiene los movimientos realizados en la cuenta.
        //Un maximo de 10
        public Response obtenerMovimientos()
        {
            try
            {
                int num1 = 10;
                int num2 = 0;

                string endpoint = $"/WCS/api/transaction?HowMany={num1}&FromTo={num2}";
                Response response = Get(endpoint, new {num1,num2});
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Obtiene el movimiento en detalle
        //Pasarle el id de movimiento
        public Response obtenerDetalleMovimiento(int id)
        {
            try
            {
                string endpoint = $"/WCS/api/transaction/{id}";
                Response response = Get(endpoint, id);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
