﻿using BaseFramework.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api
{
    public class SessionClient : BaseClient
    {
        public SessionClient(string baseUrl, string token = "") : base(baseUrl, token)
        { }

        //Iniciar Sesion
        public Response Login(string user, string password)
        {
            try
            {
                string endpoint = "/WCS/api/session/login";
                Response response = Post(endpoint, new { user, password });
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
