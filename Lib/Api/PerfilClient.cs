﻿using BaseFramework.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api
{
    internal class PerfilClient : BaseClient
    {
        public PerfilClient(string baseUrl, string token = "") : base(baseUrl, token)
        {}

        //Cambiar la contraseña una vez iniciada la sesion.
        public Response cambiarContrasenia(string Email, string NewPass)
        {
            try
            {
                string endpoint = "/WCS/api/perfil/pass";
                Response response = Post(endpoint, new { Email, NewPass });
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Envia el mail con el codigo de recupero de la cuenta.
        public Response enviarMail(string email)
        {
            try
            {
                email = "\"" + email + "\"";
                string endpoint = "/WCS/api/perfil/email";
                Response response = Post(endpoint, email);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Cambiar la contraseña una vez iniciada la sesion.
        public Response ingresarCodigoRecupero(string email, string code)
        {
            try
            {
                string endpoint = "/WCS/api/perfil/code";
                Response response = Post(endpoint, new { email, code });
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
