﻿using BaseFramework.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api
{
    public class CreditClient : BaseClient
    {
        public CreditClient(string baseUrl, string token = "") : base(baseUrl, token)
        { 
        }

        //Consulta saldo disponible
        public Response consultarSaldo()
        {
            try
            {
                string endpoint = "/WCS/api/saldo";
                Response response = Get(endpoint);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Genera la URL para cargar saldo
        public Response cargarSaldo(decimal amount)
        {
            try
            {
                string endpoint = "/WCS/api/credit";
                Response response = Post(endpoint, amount);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
