﻿namespace BaseFramework.StepsDefinitions
{
    [Binding]
    internal class ConsultasStep : Steps
    {
        private readonly ScenarioContext _scenarioContext;
        private CartClient? _cartClient;
        private CartModel _cartModel;
        private string _host;
        private string _token;
        private EnvironmentManager _environment;

        public ConsultasStep(ScenarioContext context)
        {
            _scenarioContext = context;
            _cartModel = new CartModel();
            _host = _scenarioContext.Get<string>("host");
            _token = _scenarioContext.Get<string>("TOKEN");
            _environment = _scenarioContext.Get<EnvironmentManager>("user");
            _cartClient = new CartClient(_host, _token);
        }

        //@CI.1 -- INICIO --
        [StepDefinition(@"El usuario obtiene su numero de idConsumidor")]
        public void tiendasHabilitadas()
        {
            var response = _cartClient.saberId(_environment.User.Nombre, _environment.User.Mail).Result;

            _scenarioContext.Add("iDResponse", response);

            var respuesta = _scenarioContext.Get<Response>("iDResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(respuesta.Content)!;

            Console.WriteLine(_cartModel.Body.Body);
        }
        //@CI.1 -- FIN --
    }
}
