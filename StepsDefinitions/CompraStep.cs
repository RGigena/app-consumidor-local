﻿namespace BaseFramework.StepsDefinitions
{
    [Binding]
    internal class CompraStep : Steps
    {
        private Response _response;
        private readonly ScenarioContext _scenarioContext;
        private UserModel _userModel;
        private CartClient? _cartClient;
        private CartModel _cartModel;
        private string _host;
        private string _token;
        private EnvironmentManager _environment;
        private Random random = new Random();
        public static string? IdProducto { get; set; }

        public CompraStep(ScenarioContext context) 
        {
            _response = new Response();
            _scenarioContext = context;
            _userModel = new UserModel();
            _cartModel = new CartModel();
            _host = _scenarioContext.Get<string>("host");
            _token = _scenarioContext.Get<string>("TOKEN");
            _environment = _scenarioContext.Get<EnvironmentManager>("user");
            _cartClient = new CartClient(_host, _token);
        }

        [When(@"Se escanea el QR del producto seleccionado")]
        public void qrSeleccion() 
        {
            //SE UTILIZAN QR HARCODEADOS
            //SON LOS PRODUCTOS DISPONIBLES EN LA TIENDAS DE PRUEBA PARA CADA ENTORNO
            //SE HIZO ASI, PARA QUE ELIJA AL AZAR ENTRE LOS PRODUCTOS DISPONIBLES.
            //ESTAN SEPARADOS POR ENTORNOS

            //SE USA LA VARIABLE HOSTTEST DECLARADA EN LA CLASE DE LOGINSTEP.
            //EN ELLA SE GUARDÓ EL HOST QUE SE ESTA UTILIZANDO EN ESE MOMENTO.
            if(LoginStep.HostTest != "https://test.srvcontrol.com.ar")
            {
                //QR TEST
                string[] qrValues = {
                        "https://cg-qr.com?rR09zVgb7Rjaer_vqUqc-w==",
                        "https://cg-qr.com?kv33bVPPoKKFDjumv6iU1Q==",
                        "https://cg-qr.com?VeFS1TlesYfjmbubneex4A=="
                };

                //SE ELIJE QR DE FORMA RANDOM
                string qr;
                string qrRandom = qrValues[random.Next(qrValues.Length)];
                qr = qrRandom;

                _response = _cartClient.obtenerQr(qr);

                if (!_scenarioContext.ContainsKey("cartResponse"))
                {
                    _scenarioContext.Add("cartResponse", _response);
                }
                else
                {
                    _scenarioContext["cartResponse"] = _response;
                }
            }
            else
            {
                //QR DEV
                string[] qrValues = {
                    "https://cg-qr.com?uzpXLaQc0oHffY4ZgEeKbQ==",
                    "https://cg-qr.com?m5X6WtVVQyQ24jerMa9OEA==",
                    "https://cg-qr.com?r7vQz9ElxLxnnVywExFByA==",
                    "https://cg-qr.com?lg9whjgh6UcMe1i24zwI2A=="
                };

                string qr;
                string qrRandom = qrValues[random.Next(qrValues.Length)];
                qr = qrRandom;

                _response = _cartClient.obtenerQr(qr);

                if (!_scenarioContext.ContainsKey("cartResponse"))
                {
                    _scenarioContext.Add("cartResponse", _response);
                }
                else
                {
                    _scenarioContext["cartResponse"] = _response;
                }
            }
        }

        [When(@"Se escanea el QR del segundo producto seleccionado")]
        public void qrSeleccionSegundoProducto()
        {
            //SE HACE REFERENCIA AL PASO ANTERIOR
            //PARA NO REPETIRLOS.
            When("Se escanea el QR del producto seleccionado");
        }

        [StepDefinition(@"Se agrega producto al carrito")]
        public void agregarProducto()
        {
            Assert.AreEqual(200, _scenarioContext.Get<Response>("cartResponse").StatusCode, "Ocurrio un error al seleccionar el producto");

            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

            //SE LE DA VALOR A LA VARIABLE IdProducto
            IdProducto = _userModel.Body.id;

            Console.WriteLine("Producto seleccionado: " + _userModel.Body.id);

            //SE SELECCIONA UN NUMERO DE UNIDADES DE FORMA RANDOM
            int numeroRandom = random.Next(1, 3);

            _cartModel.unidades = numeroRandom.ToString();

            Console.WriteLine("Cantidad de unidades cargadas: " + _cartModel.unidades);
        }

        [Then(@"Articulo Cargado")]
        public void articuloCargado()
        {
            _response = _cartClient.agregarProducto(IdProducto, _cartModel.unidades!);

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }

            Assert.AreEqual(200, _scenarioContext.Get<Response>("cartResponse").StatusCode, "Ocurrio un error al guardar el producto");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            Console.WriteLine("Status: " + _cartModel.status + " Producto Cargado");
        }

        [StepDefinition(@"El usuario procede a finalizar la compra")]
        public void finalizarCompra()
        {
            _response = _cartClient.pagarProducto();

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }
        }

        [Then(@"Compra Finalizada")]
        public void compraFinalizada()
        {
            Assert.AreEqual(200, _scenarioContext.Get<Response>("cartResponse").StatusCode, "Ocurrio un error al guardar el producto");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            Console.WriteLine("Status: " + _cartModel.message);
        }
    }
}
