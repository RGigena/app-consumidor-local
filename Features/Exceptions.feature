@Exceptions
Feature: Exceptions


@E.1
Scenario: Sin permisos de consumo de producto
	Given El usuario no tiene permisos de consumo para el producto A
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto A
	Then El usuario no tiene permitido consumir el producto A

@E.2
Scenario: Sin permiso de consumo en una tienda
	Given El usuario No tiene permisos de consumo en la tienda A
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto A de la tienda A
	Then El usuario no puede consumir en la tienda A

@E.3
Scenario: Carga de credito electronica deshabilitada
	Given Se limita al usuario la carga electronica de credito
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	When El usuario procede a realizar una carga de saldo
	Then El usuario no esta habilitado para la carga electronica

@E.4
Scenario: Cuenta sin saldo
	Given Se le quita al usuario el saldo
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario procede a finalizar la compra
	Then El usuario no tiene saldo para realizar la compra

@E.5
Scenario: Limite de consumo de producto
	Given Se limita la cantidad de consumo del producto A al usuario A
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto limitado A
	And El usuario agrega al carrito una cantidad mayor a la permitida del producto A
	Then El usuario supero el limite del producto A

@E.6
Scenario: Consumo no permitido en dos tiendas simultaneas
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When se escanea el QR del producto A de la tienda A
	Then se agrega el producto A de la tienda A
	When se escanea el QR del producto B de la tienda B
	Then el usuario no puede consumir en dos tiendas a la vez

@E.7
Scenario: Pago duplicado
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario procede a finalizar la compra
	Then Compra Finalizada
	When El usuario procede a finalizar la compra nuevamente
	Then El carrito no existe

@E.8
Scenario: Abonar el carrito y despues vaciarlo
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario procede a finalizar la compra
	Then Compra Finalizada
	When El usuario procede a vaciar el carrito
	Then El usuario no puede vaciar el carrito

@E.9
Scenario: Username Erroneo 
	Given El usuario se loguea en la app con user name incorrecto
	Then El usuario corrige el usuario
	And Inicia sesion correctamente

@E.10
Scenario: Contrasenia erronea
	Given El usuario se loguea en la app con pass incorrecta
	Then El usuario corrige la contrasenia
	And Inicia sesion correctamente

@E.11
Scenario: Cuenta suspendida
	Given Se suspende la cuenta del usuario
	When El usuario se loguea en la app
	Then El usuario recibe el mensaje de cuenta suspendida

@E.12
Scenario: Sesion Iniciada + Cuenta suspendida
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se suspende la cuenta del usuario
	And El usuario chequea el saldo disponible
	Then El usuario recibe el mensaje de cuenta suspendida