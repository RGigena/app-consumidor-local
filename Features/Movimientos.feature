@Movimientos
Feature: Movimientos

@MC.1
Scenario: Obtener Movimientos
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario ingresa a ver el listado de movimientos realizados
	Then El usuario obtiene una lista con los movimientos realizados

@MC.2
Scenario: Obtener Movimientos + Ver en detalle
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario ingresa a ver el listado de movimientos realizados
	Then El usuario obtiene una lista con los movimientos realizados
	When El usuario selecciona un movimiento del listado para ver en detalle
	Then Se muestra el movimiento en detalle