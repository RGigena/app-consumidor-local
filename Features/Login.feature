﻿@Login
Feature: Login

@LVC.1
Scenario: Login
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente

@LVC.2
Scenario: Cambiar pass
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario cambia la contrasenia
	Then La contrasenia se actualiza correctamente
	When El usuario se loguea en la app
	Then Inicia sesion correctamente

@LVC.3
Scenario: Olvidó la contrasenia
	Given El usuario olvido su contrasenia
	When El usuario recupera su contrasenia
	And El usuario recibe el codigo de recupero
	And El usuario valida codigo de recupero
	Then El usuario restablece la contrasenia

