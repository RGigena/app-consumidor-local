@Compra
Feature: Compra

@OkRun
Scenario: Realizar compra
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario procede a finalizar la compra
	Then Compra Finalizada
	When El usuario ingresa a ver el listado de movimientos realizados
	Then El usuario obtiene una lista con los movimientos realizados