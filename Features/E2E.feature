@E2E
Feature: E2E

@OkRun
Scenario: E2E.1
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario cambia la contrasenia
	Then La contrasenia se actualiza correctamente
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario procede a finalizar la compra
	Then Compra Finalizada
	When El usuario ingresa a ver el listado de movimientos realizados
	Then El usuario obtiene una lista con los movimientos realizados

@OkRun.2
Scenario: E2E.2
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario cambia la contrasenia
	Then La contrasenia se actualiza correctamente
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario controla el total a pagar
	And El usuario procede a finalizar la compra
	Then Compra Finalizada
	When El usuario ingresa a ver el listado de movimientos realizados
	Then El usuario obtiene una lista con los movimientos realizados

@OkRun.3
Scenario: E2E.3
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario cambia la contrasenia
	Then La contrasenia se actualiza correctamente
	When El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	#PRODUCTO #1
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	#PRODUCTO #2
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario controla el total a pagar
	And El usuario quita las unidades del producto
	Then El producto fue retirado del carrito
	When El usuario controla el total a pagar
	And El usuario procede a finalizar la compra
	Then Compra Finalizada
	When El usuario ingresa a ver el listado de movimientos realizados
	Then El usuario obtiene una lista con los movimientos realizados